import json
import time

import boto3
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from core.engine import ParserSite
from core.models import BaseSite


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print('START COMMAND')
        startTime = time.time()
        sns = boto3.client('sns', region_name='us-west-2',
                           aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                           aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        for site in BaseSite.objects.all():
            response = sns.publish(
                TopicArn='arn:aws:sns:us-west-2:034672374092:createlink',
                Message=json.dumps({"id": site.id, "rss": site.rss}),
                Subject='createlink'
            )
            print(response)
        print('END COMMAND', time.time() - startTime)