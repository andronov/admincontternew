import time
from django.core.management.base import BaseCommand, CommandError

from core.engine import ParserSite
from core.models import BaseSite, UserWallSettings, BaseLink
from core.utils import calculation_to_word


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print('START COMMAND')
        starttime = time.time()
        """
        id = 2
        txt = 'Analyst: Apple could be exploring changes to Touch ID for its 2017 iPhone'
        walls = UserWallSettings.objects.raw("SELECT id, user_id, wall_id "
                                             "FROM user_wall_settings WHERE is_active = True "
                                             "AND sources @> '{\"sites\": [%s]}' AND ("
                                             "words != '{\"excluding\": [], \"matching\": []}' OR "
                                             "words != '{\"excluding\": [\"\"], \"matching\": [\"\"]}');" % str(id))
        #print('walls', walls)
        for wall in walls:
            #print('wall', wall.id, wall.words)
            nxt = calculation_to_word(txt, wall.words.get('matching', []), wall.words.get('excluding', []))
            print(nxt)
        """
        for base_link in BaseLink.objects.filter(type=1):
            parser = ParserSite(base_link.site.id)
            parser.base_site = base_link.site
            parser.create_user_link(base_link)

        print('END COMMAND', time.time() - starttime)