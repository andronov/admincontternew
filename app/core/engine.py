import datetime
import json
import os
import re
import uuid

import boto3
from django.conf import settings
from wand.image import Image
import requests
from bs4 import BeautifulSoup
from core.models import *
from slugify import slugify
from django.db import connection
import psycopg2.extensions
import feedparser

from core.utils import calculation_to_word


class ParserSite(object):

    def __init__(self, id):
        self.id = id
        self.base_site = None
        self.path = "C:\project\contter\/app\media\{}".format(str(id))

    def run(self):
        self.init_client()
        self.base_site = BaseSite.objects.get(id=self.id)
        self.feedparser()

    def init_client(self):
        self.client_s3 = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

    @staticmethod
    def get_time_from_feed(entry):
        created = datetime.datetime.utcnow()
        try:
            created = datetime.datetime(*entry.published_parsed[:6])
        except:
            pass

        return created

    def feedparser(self):
        feed = feedparser.parse(self.base_site.rss)
        diff = [f.url for f in BaseLink.objects.filter(url__in=[entry.link for entry in feed.entries])]
        diff = list(set([f.link for f in feed.entries]) - set(diff))
        for entry in feed.entries:
            if entry.link in diff:
                created = self.get_time_from_feed(entry)
                base_link = self.create_link(entry, created)
                print('base_link', base_link.id)
                if base_link:
                    self.create_user_link(base_link)

    def create_user_link(self, base_link):
        """
        ДОбавляем к юзеру ссылку
        :param base_link:
        :return:
        """
        walls = UserWallSettings.objects.raw("SELECT id, user_id, wall_id "
                                             "FROM user_wall_settings WHERE is_active = True "
                                             "AND sources @> '{\"sites\": [%s]}' AND ("
                                             "words = '{\"excluding\": [], \"matching\": []}' OR "
                                             "words = '{\"excluding\": [\"\"], \"matching\": [\"\"]}');" % str(self.id))

        for wall in walls:
            link, created = UserWallLink.objects.get_or_create(user=wall.user, wall=wall.wall, site=self.base_site,
                                                               link=base_link, defaults={"created": base_link.created,
                                                                                         "is_active": True})

            if not created:
                continue

            self.notify(link, wall)

        walls = UserWallSettings.objects.raw("SELECT id, user_id, wall_id "
                                             "FROM user_wall_settings WHERE is_active = True "
                                             "AND sources @> '{\"sites\": [%s]}' AND ("
                                             "words != '{\"excluding\": [], \"matching\": []}' OR "
                                             "words != '{\"excluding\": [\"\"], \"matching\": [\"\"]}');" % str(self.id))

        print('walls', walls)
        for wall in walls:
            txt = base_link.title + ' ' + base_link.description if base_link.description else ''
            nxt = calculation_to_word(txt, wall.words.get('matching', []), wall.words.get('excluding', []))
            print(nxt)
            if not nxt:
                continue

            link, created = UserWallLink.objects.get_or_create(user=wall.user, wall=wall.wall, site=self.base_site,
                                                               link=base_link, defaults={"created": base_link.created,
                                                                                         "is_active": True})

            if not created:
               continue

            self.notify(link, wall)

    def notify(self, link, wall):
        return
        description = ''
        if link.link.description:
            description = link.link.description.replace("'", "''")
        data = {"id": int(str(link.site.id)+str(link.id)), "unique_id": link.id,
                "link_id": link.link.id, "title": link.link.title.replace("'", "''"),
                "description": description,
                "medium_img": link.link.medium_img,
                "url_path": link.link.url, "url": link.link.url, "thumbnail": link.link.thumbnail,
                "type": link.link.type, "site": {"name": self.base_site.name,
                                                 "short_url": self.base_site.short_url,
                                                 "favicon": self.base_site.favicon, "image": self.base_site.image},
                "created": link.created.timestamp(),
                "wall_id": wall.wall.id,
                "wallType": wall.wall.type,
                "site_id": link.site.id,
                "liked": False,
                "shared": False,
                "read_browser": True,
                "user": {"username": link.user.username, "first_name": link.user.first_name}}
        cmd = json.dumps({"cmd": "INSERT", "type": "USER", "user": link.user.id, "name": "ws_new_links",
                          "data": data})
        notify = "NOTIFY users, '"+str(cmd)+"'"
        crs = connection.cursor()
        connection.connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        crs.execute(notify)
        crs.close()

        return

    @staticmethod
    def grouplen(sequence, chunk_size):
        return list(zip(*[iter(sequence)] * chunk_size))

    def create_link(self, entry, created):
        """
        Создаем ссылку
        :param entry:
        :param created:
        :return:
        """
        request = requests.get(entry['link'], headers=self.get_request_headers())
        soup = BeautifulSoup(request.text, "html.parser")

        title = None
        try:
            try:
                title = soup.find("title").text
            finally:
                title = soup.find("meta", {"property": "og:title"})['content']
        except:
            pass

        description = None
        try:
            try:
                description = soup.find("meta", {"name": "description"})['content']
            finally:
                description = soup.find("meta", {"property": "og:description"})['content']
        except:
            pass

        keywords = None
        try:
            try:
                keywords = soup.find("meta", {"name": "keywords"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass

        img = None
        try:
            try:
                img = soup.find("meta", {"property": "og:image"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass

        filename, filename_thumb, json_img = None, None, None
        if img:
            try:
                filename, filename_thumb, json_img = self.create_image(title, img)
            except Exception as e:
                print('image', e)

        return BaseLink.objects.create(type=1, site=self.base_site, title=title, description=description,
                                       keywords=keywords, url=entry.link, thumbnail=filename_thumb,
                                       medium_img=filename, json_img=json_img, created=created)

    def create_image(self, title, img):
        json_img = {}
        r = requests.get(img, headers=self.get_request_headers())
        name = slugify(title)
        headers = r.headers.get('content-type', 'image/jpeg')

        result = re.findall(r'(image/png)|(image/jpeg)|(image/gif)', headers)
        res = ''.join(result[0])
        mime = re.findall(r'/\w*', res)[0].replace('/', '')
        img_name = '{}_image.{}'.format(name, mime)
        img_thumb_name = '{}_image__thumb.{}'.format(name, mime)
        path_img = self.create_s3_path(img_name)
        path_img_thumb = self.create_s3_path(img_thumb_name)

        filename, short_name = self.create_unique_name_file(img_name)
        f = open(filename, 'wb')
        f.write(r.content)
        f.close()

        with open(filename, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path_img, ExtraArgs={'ContentType': res})

        filename_thumb, short_thumb_name = self.create_unique_name_file(img_thumb_name)
        with Image(filename=filename) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = mime
            img.save(filename=filename_thumb)

        with open(filename_thumb, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path_img_thumb, ExtraArgs={'ContentType': res})

        os.remove(filename)
        os.remove(filename_thumb)

        return path_img, path_img_thumb, json_img

    def create_s3_path(self, name):

        return '{}/{}/{}'.format(datetime.datetime.utcnow().strftime("%Y%m%d"), str(self.id), name)

    def create_unique_name_file(self, unique_name):
        """
        Создаем уникальное имя для файла.
        :param path: путь до папки с файлами.
        :param name: название файла.
        :return:
        """
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        f = os.path.join(self.path, unique_name)
        if os.path.isfile(f):
            unique_name = str(uuid.uuid4())[:12] + "__" + unique_name
            return self.create_unique_name_file(unique_name)
        return f, unique_name

    def get_request_headers(self):
        return {
            "Accept-Language": "en-US,en;q=0.5",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.28 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Referer": "https://www.reddit.com",
            "Connection": "keep-alive"
        }