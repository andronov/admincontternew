from django.conf.urls import url

from core.views import CoreView, CoreAddView, CoreAddImgView

urlpatterns_core = [
   url(r'^$', CoreView.as_view()),
   url(r'^add-img', CoreAddImgView.as_view()),
   url(r'^add', CoreAddView.as_view()),
]