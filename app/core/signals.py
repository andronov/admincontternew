from django.db.models.signals import post_save
from django.dispatch import receiver
from djcelery.models import IntervalSchedule, PeriodicTask

from core.models import BaseSite


@receiver(post_save, sender=BaseSite)
def add_after_base_site_to_celery(sender, **kwargs):
    obj = kwargs["instance"]
    interval = IntervalSchedule.objects.get(id=1)

    name = 'create_links ' + str(obj.id)
    ar = '['+str(obj.id)+']'

    period, created = PeriodicTask.objects.get_or_create(name=name, defaults={"task": 'core.tasks.create_links',
                                                                              "interval": interval,
                                                                              "args": ar, "enabled": obj.is_active})
    if not created:
        period.is_active = obj.is_active
        period.save()
    """
    if kwargs["created"]:

        interval = IntervalSchedule.objects.get(id=3)

        name = 'create_links ' + str(obj.id)
        ar = '['+str(obj.id)+']'

        PeriodicTask.objects.create(name=name, task='core.tasks.create_links', interval=interval,
                                    args=ar, enabled=obj.is_active)
    else:
        name = 'create_links ' + str(obj.id)

        period = PeriodicTask.objects.get(name=name)
        period.enabled = obj.is_active
        period.save()
    """

