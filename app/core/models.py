from django.contrib.postgres.fields import JSONField
from django.db import models
#from django.db.models.signals import post_save
#from djcelery.models import IntervalSchedule, PeriodicTask


class BaseModel(models.Model):
    is_active = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(BaseModel):
    TYPE_TYPES = (
        (1, u'public'),
        (2, u'private')
    )

    username = models.CharField(max_length=5000, blank=True)
    first_name = models.CharField(max_length=5000, blank=True)
    last_name = models.CharField(max_length=5000, blank=True)
    password = models.CharField(max_length=5000, blank=True)
    email = models.CharField(max_length=5000, blank=True)

    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)

    display_name = models.CharField(max_length=5000, blank=True)

    twitter = models.BigIntegerField(blank=True, null=True)
    twitter_img = models.CharField(max_length=5000, blank=True)

    description = models.CharField(max_length=5000, blank=True)
    avatar = models.CharField(max_length=5000, blank=True)
    color = models.CharField(max_length=5000, blank=True)

    new = models.BooleanField(default=True)

    last_visit = models.DateTimeField(blank=True, null=True)
    city = models.CharField(max_length=5000, blank=True)

    slug = models.CharField(max_length=5000, blank=True)
    official = models.BooleanField(default=False)
    official_info = models.CharField(max_length=5000, blank=True)

    location = models.CharField(max_length=5000, blank=True)
    website = models.CharField(max_length=5000, blank=True)
    timezone = models.CharField(max_length=5000, blank=True)
    country = models.CharField(max_length=5000, blank=True)

    followers = models.BigIntegerField(blank=True, null=True)

    is_ban = models.BooleanField(default=True)

    class Meta:
        db_table = 'users'


class BaseSite(BaseModel):
    name = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    url = models.CharField(max_length=5000, blank=True)
    short_url = models.CharField(max_length=5000, blank=True)
    rss = models.CharField(max_length=5000, blank=True)
    image = models.CharField(max_length=5000, blank=True)
    favicon = models.CharField(max_length=5000, blank=True)
    slug = models.CharField(max_length=5000, blank=True)
    color = models.CharField(max_length=5000, blank=True)
    followers = models.BigIntegerField(blank=True, null=True)
    lang = models.CharField(max_length=5000, blank=True)
    fts = models.CharField(max_length=5000, blank=True)

    parent = models.BooleanField(blank=True)
    parent_id = models.IntegerField(null=True, blank=True)

    custom = models.BooleanField(blank=True)

    class Meta:
        db_table = 'base_site'


    def image_get(self):
        if self.image and self.image.startswith('http'):
            return self.image
        else:
            return 'https://s3-us-west-2.amazonaws.com/contter/{}'.format(self.image)

    def favicon_get(self):
        if self.favicon and self.favicon.startswith('http'):
            return self.favicon
        else:
            return 'https://s3-us-west-2.amazonaws.com/contter/{}'.format(self.favicon)


class BaseLink(BaseModel):
    TYPE_TYPES = (
        (1, 'standard'),
        (2, 'dynamic'),
        (3, 'post'),
        (4, 'custom')
    )

    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)
    site = models.ForeignKey(BaseSite, blank=True, null=True, name='site')

    title = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    keywords = models.CharField(max_length=5000, blank=True)

    url = models.CharField(max_length=15000, blank=True)
    full_url = models.CharField(max_length=15000, blank=True)

    thumbnail = models.CharField(max_length=5000, blank=True)
    extra_img = models.CharField(max_length=5000, blank=True)
    small_img = models.CharField(max_length=5000, blank=True)
    medium_img = models.CharField(max_length=5000, blank=True)
    large_img = models.CharField(max_length=5000, blank=True)
    site_img = models.CharField(max_length=5000, blank=True)
    site_img_url = models.CharField(max_length=5000, blank=True)
    json_img = JSONField()

    tags = models.CharField(max_length=5000, blank=True)
    counter = models.IntegerField(null=True, blank=True, default=0)

    show = models.BooleanField(blank=True)

    class Meta:
        db_table = 'base_link'


"""
class BaseLinkInfo(BaseModel):
    link = models.ForeignKey(BaseSite, blank=True, null=True, name='link')

    html = models.TextField(null=True)
    extra = JSONField()

    class Meta:
        db_table = 'base_link_info'
"""


class UserWall(BaseModel):
    TYPE_TYPES = (
        (1, 'public'),
        (2, 'private'),
        (3, 'follow'),
        (4, 'liked'),
        (5, 'profile'),
    )

    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True, name='user')

    slug = models.CharField(max_length=5000, blank=True)
    name = models.CharField(max_length=5000, blank=True)
    color = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    icon = models.CharField(max_length=5000, blank=True)

    sort = models.IntegerField(null=True, blank=True, default=0)
    category_id = models.IntegerField(null=True, blank=True)

    thumbnail = models.CharField(max_length=5000, blank=True)
    medium_img = models.CharField(max_length=5000, blank=True)

    home = models.BooleanField(blank=True, default=False)

    class Meta:
        db_table = 'user_wall'


class UserWallSettings(BaseModel):
    user = models.ForeignKey(User, blank=True, null=True, name='user')
    wall = models.ForeignKey(UserWall, blank=True, null=True, name='wall')

    words = JSONField()
    sources = JSONField()

    realtime = models.BooleanField(blank=True, default=False)

    class Meta:
        db_table = 'user_wall_settings'


class UserWallLink(BaseModel):
    user = models.ForeignKey(User, blank=True, null=True, name='user')
    wall = models.ForeignKey(UserWall, blank=True, null=True, name='wall')
    site = models.ForeignKey(BaseSite, blank=True, null=True, name='site')
    link = models.ForeignKey(BaseLink, blank=True, null=True, name='link')

    shared_id = models.IntegerField(null=True, blank=True)
    shared = models.BooleanField(blank=True, default=False)
    shared_add = models.DateTimeField(auto_now=True)

    showed = models.BooleanField(blank=True, default=False)
    showed_add = models.DateTimeField(auto_now=True)

    is_ban = models.BooleanField(blank=True, default=False)

    counter = models.IntegerField(null=True, blank=True)

    liked = models.BooleanField(blank=True, default=False)
    liked_add = models.DateTimeField(auto_now=True)
    saved = models.BooleanField(blank=True, default=False)
    saved_add = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'user_wall_link'