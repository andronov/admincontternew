import difflib

def calculation_to_word(text1, word_filter, word_exclude):
    next = False
    txt1 = text1.replace(",", "").replace(".", "").lower().split(' ')

    if len(word_filter) > 0:
        sm_f = difflib.SequenceMatcher(None, txt1, word_filter)
        if sm_f.ratio() > 0:
            next = True
    else:
        if len(word_exclude) > 0:
            sm_e = difflib.SequenceMatcher(None, txt1, word_exclude)
            if sm_e.ratio() <= 0:
                next = True

    return next