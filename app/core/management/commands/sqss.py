import time

import boto3
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from core.engine import ParserSite
from core.models import BaseSite


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print('START COMMAND')
        sqs = boto3.resource('sqs', region_name='us-west-2',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        print('sqs', sqs)
        queue = sqs.get_queue_by_name(QueueName='columbus')

        #response = queue.send_message(MessageBody='world')

        ## The response is NOT a resource, but gives you a message ID and MD5
        #print(response.get('MessageId'))
        #print(response.get('MD5OfMessageBody'))

        for message in queue.receive_messages(
                MaxNumberOfMessages=10):
            # Get the custom author message attribute if it was set

            # Print out the body and author (if set)
            #print(dir(message))
            print('Hello, {0}!{1}'.format(message.body, message.message_id))

            # Let the queue know that the message is processed
            message.delete()

        """
        client = boto3.client(
            'sqs',
            region_name='us-west-2',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        print('client', dir(client))
        #queue = client.get_queue_url(QueueName='columbus')
        queue = client.Queue('https://us-west-2.queue.amazonaws.com/034672374092/columbus')
        print(queue)
        """
        """
        response = client.send_message(
                        QueueUrl=queue['QueueUrl'],
                        MessageBody='string',
                        DelaySeconds=123,
                        MessageAttributes={
                            'string': {
                                'StringValue': 'string',
                                'BinaryValue': b'bytes',
                                'StringListValues': [
                                    'string',
                                ],
                                'BinaryListValues': [
                                    b'bytes',
                                ],
                                'DataType': 'string'
                            }
                        },
                        MessageDeduplicationId='string',
                        MessageGroupId='string'
                    )
        print('response1', response)
        """
        print('END COMMAND')