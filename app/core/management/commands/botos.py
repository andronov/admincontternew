import time
from django.core.management.base import BaseCommand, CommandError
import datetime
import json
import os
import re
import uuid
from wand.image import Image
import requests
from slugify import slugify
from core.engine import ParserSite
from core.models import BaseSite
import boto3


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    id = 1
    path = "C:\project\contter\/app\media\{}".format(str(1))


    def create_unique_name_file(self, unique_name):
        """
        Создаем уникальное имя для файла.
        :param path: путь до папки с файлами.
        :param name: название файла.
        :return:
        """
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        f = os.path.join(self.path, unique_name)
        if os.path.isfile(f):
            unique_name = str(uuid.uuid4())[:12] + "__" + unique_name
            return self.create_unique_name_file(unique_name)
        return f, unique_name

    def get_request_headers(self):
        return {
            "Accept-Language": "en-US,en;q=0.5",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.28 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Referer": "https://www.reddit.com",
            "Connection": "keep-alive"
        }

    def create_image(self, title, img):
        json_img = {}
        r = requests.get(img, headers=self.get_request_headers())
        name = slugify(title)
        headers = r.headers.get('content-type', 'image/jpeg')

        result = re.findall(r'(image/png)|(image/jpeg)|(image/gif)', headers)
        res = ''.join(result[0])
        mime = re.findall(r'/\w*', res)[0].replace('/', '')
        img_name = '{}_image.{}'.format(name, mime)
        img_thumb_name = '{}_image__thumb.{}'.format(name, mime)
        path_img = self.create_s3_path(img_name)
        path_img_thumb = self.create_s3_path(img_thumb_name)

        filename, short_name = self.create_unique_name_file(img_name)
        f = open(filename, 'wb')
        f.write(r.content)
        f.close()

        client = boto3.client(
            's3',
            aws_access_key_id='AKIAINK7324DD7P67DSQ',#ACCESS_KEY,
            aws_secret_access_key='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',#SECRET_KEY,
            #aws_session_token=SESSION_TOKEN,
        )

        with open(filename, 'rb') as data:
            client.upload_fileobj(data, 'contter', path_img, ExtraArgs={'ContentType': res})

        filename_thumb, short_thumb_name = self.create_unique_name_file(img_thumb_name)
        with Image(filename=filename) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = mime
            img.save(filename=filename_thumb)

        with open(filename_thumb, 'rb') as data:
            client.upload_fileobj(data, 'contter', path_img_thumb, ExtraArgs={'ContentType': res})

        os.remove(filename)
        os.remove(filename_thumb)

        return path_img, path_img_thumb, json_img

    def create_s3_path(self, name):

        return '{}/{}/{}'.format(datetime.datetime.utcnow().strftime("%Y%m%d"), str(self.id), name)

    def handle(self, *args, **options):
        print('START COMMAND')


        # format /дата/ид сайта/ид ссылки/ссылка
        # format /02022016/57/3/imagedf_thumb.jpg
        title = 'Jeffrey Earnhardt to make first career Daytona 500 start'
        img = 'http://a.espncdn.com/photo/2014/0704/rpm_g_earnhardt11_1296x729.jpg'
        path_img, path_img_thumb, json_img = self.create_image(title, img)
        print(path_img, path_img_thumb, json_img)
        #with open('hello.txt', 'rb') as data:
            #client.upload_fileobj(data, 'contter', 'hello2.txt')

        #result = client.upload_file('hello.txt', 'contter', 't/hello.txt', Callback=upload)
        #print(result)
        #client.put_bject('contter', 'hello.txt').put(Body=open('hello.txt', 'rb'))

        #for bucket in client.buckets.all():
        #    print(bucket.name)

        print('END COMMAND')