import json
import time

import boto3
from celery.task import task
from django.conf import settings

from core.engine import ParserSite
from core.models import BaseSite


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def just_print():
    print("Print from celery task")

@task
def only():
    print('only')
    print("Print from celery task")

@task
def create_links():
    print('START COMMAND')
    startTime = time.time()
    sns = boto3.client('sns', region_name='us-west-2',
                       aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                       aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
    for site in BaseSite.objects.filter(is_active=True):
        response = sns.publish(
            TopicArn='arn:aws:sns:us-west-2:034672374092:createlink',
            Message=json.dumps({"id": site.id, "rss": site.rss}),
            Subject='createlink'
        )
        print(response)
    print('END COMMAND', time.time() - startTime)