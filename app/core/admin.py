from django.contrib import admin

from core.models import *


class BaseSiteAdmin(admin.ModelAdmin):
    search_fields = ['id', 'name', 'url', 'short_url']
    list_display = ('id', 'name', 'url', 'short_url', 'rss', 'is_active', 'favicon', 'color')


class BaseLinkAdmin(admin.ModelAdmin):
    search_fields = ['id', 'url']
    list_display = ('id', 'url')


class UserWallAdmin(admin.ModelAdmin):
    search_fields = ['id', 'name']
    list_display = ('id', 'user', 'name', 'is_active')


class UserWallSettingsAdmin(admin.ModelAdmin):
    search_fields = ['id', 'user']
    list_display = ('id', 'sources', 'words', 'user', 'wall', 'is_active')


class UserWallLinkAdmin(admin.ModelAdmin):
    search_fields = ['id', 'user']
    list_display = ('id', 'user', 'wall', 'link', 'site', 'is_active')


admin.site.register(User)
admin.site.register(BaseSite, BaseSiteAdmin)
admin.site.register(BaseLink, BaseLinkAdmin)
admin.site.register(UserWall, UserWallAdmin)
admin.site.register(UserWallSettings, UserWallSettingsAdmin)
admin.site.register(UserWallLink, UserWallLinkAdmin)