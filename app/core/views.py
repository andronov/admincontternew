import datetime
import json
import os
import re
import uuid

import boto3
import requests
from django.conf import settings
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import TemplateView, ListView
from wand.image import Image

from core.models import BaseSite


class CoreView(ListView):
    template_name = 'core/main.html'
    model = BaseSite
    queryset = BaseSite.objects.all().order_by('-id')[:100]

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise Http404
        return super(CoreView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        post = self.request.POST
        print(post)
        return JsonResponse({'ok': True})

    def get_context_data(self, **kwargs):
        context = super(CoreView, self).get_context_data(**kwargs)
        print(context)
        return context


class CoreAddView(TemplateView):
    template_name = 'core/add.html'
    model = BaseSite

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise Http404
        return super(CoreAddView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        post = self.request.POST
        values = {}
        data = json.loads(post.get('data'))
        for dt in data:
            values[dt['name']] = dt['value']

        fts = "'{}':1A,2A,3B,4C".format(values.get('short_url', ''))
        site, created = BaseSite.objects.get_or_create(slug=values.get('slug'),
                                                       defaults={"image": values.get('original_image'),
                                                                 "favicon": values.get('original_favicon'),
                                                                 "name": values.get('name'),
                                                                 "slug": values.get('slug'),
                                                                 "description": values.get('description'),
                                                                 "rss": values.get('rss'),
                                                                 "color": values.get('color'),
                                                                 "lang": values.get('lang'),
                                                                 "url": values.get('url'),
                                                                 "short_url": values.get('short_url'),
                                                                 "fts": fts})
        return JsonResponse({'ok': True})

    def get_context_data(self, **kwargs):
        context = super(CoreAddView, self).get_context_data(**kwargs)
        print(context)
        return context


class CoreAddImgView(TemplateView):
    template_name = 'base.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise Http404
        return super(CoreAddImgView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        value = self.request.POST.get('value')
        path = self.create_image(value)
        return JsonResponse({"ok": True, "path": path})

    def create_image(self, img):
        json_img = {}
        r = requests.get(img, headers=self.get_request_headers())

        print(str(uuid.uuid4()))
        nm = img.split('/').pop()
        name = '{}-{}'.format(str(uuid.uuid4()), nm)
        path = self.create_s3_path(name)
        print(path)

        f = open(name, 'wb')
        f.write(r.content)
        f.close()

        self.client_s3 = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

        with open(name, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path, ExtraArgs={'ContentType': 'image/vnd.microsoft.icon'})

        os.remove(name)

        return path
        headers = r.headers.get('content-type', 'image/jpeg')

        result = re.findall(r'(image/png)|(image/jpeg)|(image/gif)', headers)
        res = ''.join(result[0])
        mime = re.findall(r'/\w*', res)[0].replace('/', '')
        img_name = '{}_image.{}'.format(name, mime)
        img_thumb_name = '{}_image__thumb.{}'.format(name, mime)
        path_img = self.create_s3_path(img_name)
        path_img_thumb = self.create_s3_path(img_thumb_name)

        filename, short_name = self.create_unique_name_file(img_name)
        f = open(filename, 'wb')
        f.write(r.content)
        f.close()

        with open(filename, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path_img, ExtraArgs={'ContentType': res})

        filename_thumb, short_thumb_name = self.create_unique_name_file(img_thumb_name)
        with Image(filename=filename) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = mime
            img.save(filename=filename_thumb)

        with open(filename_thumb, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path_img_thumb, ExtraArgs={'ContentType': res})

        os.remove(filename)
        os.remove(filename_thumb)

        return path_img, path_img_thumb, json_img

    def create_s3_path(self, name):

        return 'site/{}/{}'.format(datetime.datetime.utcnow().strftime("%Y%m%d"), name)

    def create_unique_name_file(self, unique_name):
        """
        Создаем уникальное имя для файла.
        :param path: путь до папки с файлами.
        :param name: название файла.
        :return:
        """
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        f = os.path.join(self.path, unique_name)
        if os.path.isfile(f):
            unique_name = str(uuid.uuid4())[:12] + "__" + unique_name
            return self.create_unique_name_file(unique_name)
        return f, unique_name

    def get_request_headers(self):
        return {
            "Accept-Language": "en-US,en;q=0.5",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.28 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Referer": "https://www.reddit.com",
            "Connection": "keep-alive"
        }

    def get_context_data(self, **kwargs):
        context = super(CoreAddImgView, self).get_context_data(**kwargs)
        print(context)
        return context